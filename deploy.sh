#!/usr/bin/env bash

NAME=pong-bot
IMAGE=furgin/pong-bot:latest

docker pull $IMAGE
if [ "$(docker ps -q -f name=$NAME)" ]; then
  COMMAND=$(docker run --rm -v /var/run/docker.sock:/var/run/docker.sock assaflavie/runlike $NAME)
  echo Stopping "${NAME}"
  docker stop "${NAME}"
  if [ "$(docker ps -aq -f status=exited -f name=$NAME)" ]; then
    docker rm $NAME
  fi
  ${COMMAND}
else
  echo "container not found: $NAME"
fi
