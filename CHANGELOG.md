# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.0.6

- patch: Countdown before serving
- patch: End game at 3 points
- patch: Reset ball after scoring
- patch: Score tracking
- patch: Speed up ball slowly

## 0.0.5

- patch: Bump Release
- patch: fixed server crash bug

## 0.0.4

- patch: Prevent challenge for players in game
- patch: Prevent invalid player names
- patch: Send newGame and endGame messages

## 0.0.3

- patch: new protocol

## 0.0.2

- patch: Download proto file

## 0.0.1

- patch: Initial release
- patch: Initial release

