import React from 'react';
import {Event} from 'react-socket-io';

export class Results extends React.Component {

    constructor(props) {
        super(props);
        this.onResults = this.onResults.bind(this);
        this.onReset = this.onReset.bind(this);
        this.state = {
            results: {}
        }
    }

    onResults(results) {
        console.log("results", results);
        this.setState({results: results})
    }

    onReset() {
        console.log("reset");
        this.setState({results: {}})
    }

    render() {
        return (
            <div>
                <Event event='reset' handler={this.onReset}/>
                <Event event='results' handler={this.onResults}/>
                <h1>Results</h1>
                <ul>
                    {Object.keys(this.state.results)
                        .map(key => <li key={key}>
                            <div>
                                <div>{key}</div>
                                <div>wins: {this.state.results[key].wins}</div>
                                <div>losses: {this.state.results[key].losses}</div>
                                <div>disconnects: {this.state.results[key].disconnects}</div>
                            </div>
                        </li>)
                    }
                </ul>
            </div>
        );
    }
}



