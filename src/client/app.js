import React from 'react';
import {Socket} from 'react-socket-io';
import {PlayerList} from './playerList'
import {BotState} from "./botState";
import {Results} from "./results";

const version = require('../version');

const uri = 'http://localhost:3001';
const options = {transports: ['polling']};

export class App extends React.Component {
    render() {
        return (
            <Socket uri={uri} options={options}>
                <div>
                    <BotState/>
                    <PlayerList/>
                    <Results/>
                    version: {version}
                </div>
            </Socket>
        );
    }
}
