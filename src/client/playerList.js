import React from 'react';
import {Event, SocketContext} from 'react-socket-io';

export class PlayerList extends React.Component {

    constructor(props) {
        super(props);
        this.onConnected = this.onConnected.bind(this);
        this.onDisconnected = this.onDisconnected.bind(this);
        this.onReset = this.onReset.bind(this);
        this.challenge = this.challengePlayer.bind(this);
        this.state = {
            players: []
        }
    }

    onConnected(connected) {
        console.log("connected", connected);
        this.setState({players: this.state.players.concat([connected.name])})
    }

    onDisconnected(disconnected) {
        console.log("disconnected", disconnected);
        this.setState({players: this.state.players.filter(n => n !== disconnected.name)})
    }

    onReset() {
        console.log("reset");
        this.setState({players: []})
    }

    challengePlayer(playerName) {
        console.log("challengePlayer: "+playerName);
        this.context.emit('challenge',playerName);
    }

    render() {
        return (
            <div>
                <Event event='reset' handler={this.onReset}/>
                <Event event='connected' handler={this.onConnected}/>
                <Event event='disconnected' handler={this.onDisconnected}/>
                <h1>Players</h1>
                <ul>
                    {this.state.players.map(p => <li key={p}>{p} <button onClick={()=>this.challengePlayer(p)}>Challenge</button></li>)}
                </ul>
            </div>
        );
    }
}

PlayerList.contextType = SocketContext;


