import React from 'react';
import {Event, SocketContext} from 'react-socket-io';

export class BotState extends React.Component {

    constructor(props) {
        super(props);
        this.onState = this.onState.bind(this);
        this.state = {
            state: "unknown"
        }
    }

    onState(state) {
        console.log("state", state);
        this.setState({state: state})
    }

    render() {
        return (
            <div>
                <Event event='state' handler={this.onState}/>
                <h1>State: {this.state.state}</h1>
            </div>
        );
    }
}


