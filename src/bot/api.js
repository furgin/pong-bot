const express = require('express')
    , app = express()
    , EventEmitter = require('events');

class HttpServer extends EventEmitter {

    connections = [];

    constructor(port, pongClient) {
        super();

        app.use(express.static('build'))

        const server = app.listen(port, () => console.log(`BOT listening at http://localhost:${port}`))

        const shutDown = () => {
            console.log('Received kill signal, shutting down gracefully');
            server.close(() => {
                console.log('Closed out remaining connections');
                process.exit(0);
            });

            setTimeout(() => {
                console.error('Could not close connections in time, forcefully shutting down');
                process.exit(1);
            }, 2000);

            this.connections.forEach(curr => curr.end());
            setTimeout(() => this.connections.forEach(curr => curr.destroy()), 1000);
        }

        server.on('connection', connection => {
            this.connections.push(connection);
            connection.on('close', () => this.connections = this.connections.filter(curr => curr !== connection));
        });

        process.on('SIGTERM', shutDown);
        process.on('SIGINT', shutDown);

        this.io = require('socket.io')(server);
        this.io.on('connection', (socket) => {
            console.log('a user connected');

            this.io.emit('reset');

            this.io.emit('state', pongClient.state);

            pongClient.opponents.forEach((o) => {
                this.io.emit('connected', {name: o});
            })

            this.io.emit('results', pongClient.results);

            socket.on('challenge', (playerName)=>{
                pongClient.emit('challenge', playerName);
            })

            socket.on('disconnect', () => {
                console.log('user disconnected');
            });
        });

        this.on('connected', (connected) => {
            console.log('connected', connected)
            this.io.emit('connected', connected);
        })

        this.on('disconnected', (disconnected) => {
            this.io.emit('disconnected', disconnected);
        })

        this.on('state', (state) => {
            this.io.emit('state', state);
        })

        this.on('results', (state) => {
            this.io.emit('results', state);
        })
    }
}

module.exports = HttpServer