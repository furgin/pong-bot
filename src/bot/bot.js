const grpc = require("grpc")
    , protoLoader = require('@grpc/proto-loader')
    , predictor = require('./predictor')
    , Victor = require('victor')
    , HttpServer = require('./api')
    , EventEmitter = require('events')
    , version = require('../version')

require('dotenv').config();

const PROTO_PATH = process.env.PROTO_PATH || `../pong-client/Assets/Network/Protocol.proto`;
const PONG_HOST = process.env.PONG_HOST || `localhost`;
const AUTO_INVITE = (process.env.AUTO_INVITE === "true");
const protocolDefinition = protoLoader.loadSync(PROTO_PATH, {
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true
});
const protocolDescriptor = grpc.loadPackageDefinition(protocolDefinition);

console.log("VERSION: " + version);
console.log("AUTO_INVITE: " + AUTO_INVITE);
console.log("PONG_HOST: " + PONG_HOST);

const states = {
    IDLE: 'idle',
    PLAYING: 'playing'
}

const otherPlayer = (player) => {
    if (player === "player1") {
        return "player2";
    } else if (player === "player2") {
        return "player1";
    } else {
        return "unknown";
    }
}

class PongClient extends EventEmitter {

    opponents = [];
    state = states.IDLE;
    currentPlayer = "unknown";
    results = {};

    constructor(props) {
        super(props);
        this.playerName = props.playerName.toUpperCase();

        console.log("playerName: " + this.playerName);

        const client = new protocolDescriptor.pong.PongService(`${PONG_HOST}:50051`,
            grpc.credentials.createInsecure());
        const call = client.connect();
        call.write({connected: {name: this.playerName}});

        call.on('data', (message) => {
            switch (message.payload) {
                case "endGame":
                    const endGame = message.endGame;
                    if (this.state === states.PLAYING && endGame[this.currentPlayer] === playerName) {
                        const opponent = endGame[otherPlayer(this.currentPlayer)];
                        if (!this.results[opponent]) {
                            this.results[opponent] = {wins: 0, losses: 0, disconnects: 0};
                        }
                        console.log("game over: " + endGame.message);
                        if (endGame.message === this.playerName + " WON") {
                            this.results[opponent].wins++;
                        } else if(endGame.message === opponent + " WON") {
                            this.results[opponent].losses++;
                        } else {
                            this.results[opponent].disconnects++;
                        }
                        this.state = states.IDLE;
                        http.emit('state', this.state);
                        http.emit('results', this.results);
                    }
                    break;
                case "newGame":
                    if (this.state === states.IDLE) {
                        const newGame = message.newGame;
                        console.log(newGame);
                        if (newGame.player1 === this.playerName) {
                            this.currentPlayer = "player1";
                        }
                        if (newGame.player2 === this.playerName) {
                            this.currentPlayer = "player2";
                        }
                        if (this.currentPlayer !== 'unknown') {
                            console.log("I am " + this.currentPlayer);
                            this.state = states.PLAYING;
                            http.emit('state', this.state);
                        } else {
                            console.log("I am not in this game");
                        }
                    }
                    break;
                case "gameState":
                    if (this.state === states.PLAYING) {
                        const gameState = message.gameState;
                        const player = gameState[this.currentPlayer];
                        if (player) {

                            const paddlePosition = Victor.fromObject(player.position);
                            const paddleVelocity = Victor.fromObject(player.velocity);
                            const ballPosition = Victor.fromObject(gameState.ballPosition);
                            const ballVelocity = Victor.fromObject(gameState.ballVelocity);

                            if (ballVelocity.length() > 0) {
                                let diff = paddlePosition.x - ballPosition.x;
                                let direction = Math.sign(diff);
                                if (Math.sign(ballVelocity.x) === direction) {
                                    const prediction = predictor(ballPosition, ballVelocity, paddlePosition);
                                    if (prediction) {
                                        let moveDiff = prediction.y - paddlePosition.y;
                                        const moveDirection = prediction.y < paddlePosition.y ? -1 : 1;
                                        if (Math.abs(moveDiff) > 0.5 && paddleVelocity.y === 0) {
                                            call.write({move: {direction: moveDirection}});
                                            setTimeout(() => {
                                                call.write({move: {direction: 0}})
                                            }, 100);
                                        }
                                    }

                                } else {
                                    if (paddleVelocity.x !== 0) {
                                        call.write({move: {direction: 0}})
                                    }
                                }

                            }
                        } else {
                            console.log("cant find player")
                        }
                    }
                    break;
                case "connected":
                    console.log("player connected: " + message.connected.name);
                    this.opponents.push(message.connected.name);
                    http.emit("connected", message.connected)
                    if (AUTO_INVITE && message.connected.name !== this.playerName) {
                        console.log("Issue game challenge to new player: " + message.connected.name);
                        call.write({challenge: {player1: this.playerName, player2: message.connected.name}});
                    }
                    break;
                case "disconnected":
                    console.log("player disconnected: " + message.disconnected.name);
                    this.opponents = this.opponents.filter(o => o !== message.disconnected.name);
                    http.emit("disconnected", message.disconnected)
                    break;
                case "challenge":
                    call.write({accept: {player1: message.challenge.player1, player2: message.challenge.player2}});
                    break;
                case "error":
                    console.log(message);
                    break;
            }
        });

        call.on('end', () => {
            console.log("stream: finished");
        });

        call.on('error', (e) => {
            console.log("error: ", e);
        });

        this.on('challenge', (playerName) => {
            if (this.state === states.IDLE && playerName !== this.playerName) {
                console.log('challenge: ', playerName);
                call.write({challenge: {player1: this.playerName, player2: playerName}});
            }
        })

    }

}

let playerName = process.env.BOT_NAME || process.argv[2].toUpperCase();

const http = new HttpServer(3001, new PongClient({playerName}));

