const Victor = require('victor');
const {createCanvas} = require('canvas');
const fs = require('fs');

const drawImage = false;
let imageNumber = 1;

const predict = (ballPosition, ballVelocity, paddlePosition, currentPlayer) => {

    let diff = paddlePosition.x - ballPosition.x;
    let direction = Math.sign(diff);

    let position = ballPosition;
    let done = false;
    let angle = ballVelocity.angle();
    let intersect;

    let points = [];
    let angles = [];
    let hitWall = false;
    let iteration = 0;

    const rebound = (angle) => {
        return -angle;
    };

    while (!done) {

        let dist = Math.abs(paddlePosition.x - position.x);

        // opp = dist * tan(angle)
        let opp = direction * dist * Math.tan(angle);
        intersect = position.y + opp;

        // console.log("iteration:" + iteration + " angle: " + angle + " " + position + " " + intersect);
        iteration++;
        if (iteration > 10) {
            return undefined;
        }

        // predict new position after hitting wall
        if (intersect < -4.5) {
            let opp = ballPosition.y + 4.5;
            // dist = opp / Math.tan(angle)
            dist = opp / Math.tan(angle);
            // console.log("rebound on top wall at " + (position.x - dist))
            position = Victor.fromObject({x: position.x - dist, y: -4.5});
            angle = rebound(angle);
            hitWall = true;
            points.push(position);
            angles.push(angle);
        } else if (intersect > 4.5) {
            // dist = opp / Math.tan(angle)
            let opp = ballPosition.y - 4.5;
            dist = (opp) / Math.tan(angle);
            // console.log("rebound on bottom wall at " + (position.x - dist))
            position = Victor.fromObject({x: position.x - dist, y: 4.5});
            angle = rebound(angle);
            hitWall = true;
            points.push(position);
            angles.push(angle);
        } else {
            points.push(new Victor(paddlePosition.x, intersect));
            done = true;
        }
        // done = true;
    }

    const intersectPoint = new Victor(paddlePosition.x, intersect);

    if (drawImage) {

        let tag = `${currentPlayer}-${imageNumber}`;
        imageNumber++;

        const scale = 20;
        const w = 16 * scale;
        const h = 9 * scale;
        const canvas = createCanvas(w, h);
        const ctx = canvas.getContext('2d');

        const x = (v) => -v * scale + w / 2;
        const y = (v) => v * scale + h / 2;

        // draw path
        ctx.beginPath();
        ctx.lineWidth = 5;
        ctx.strokeStyle = "#00a";
        ctx.moveTo(x(ballPosition.x), y(ballPosition.y));
        points.forEach((p) => {
            ctx.lineTo(x(p.x), y(p.y));
        });
        ctx.stroke();

        // draw points
        points.forEach((p) => {
            ctx.beginPath();
            ctx.fillStyle = "#0a0";
            ctx.arc(x(p.x), y(p.y), 5, 0, 2 * Math.PI);
            ctx.fill();
        });

        const draw_angle = (p, a) => {
            if (a > 0) {
                ctx.arc(x(p.x), y(p.y), 15, -a, 0, false);
            } else {
                ctx.arc(x(p.x), y(p.y), 15, -a, 0, true);
            }
        };

        // draw angles
        ctx.lineWidth = 2;
        ctx.strokeStyle = "#0a0";
        angles.forEach((a, i) => {
            ctx.beginPath();
            let p = points[i];
            draw_angle(p, a);
            ctx.stroke();
        });

        // draw ball position
        ctx.beginPath();
        ctx.fillStyle = "#f00";
        ctx.arc(x(ballPosition.x), y(ballPosition.y), 5, 0, 2 * Math.PI);
        ctx.fill();

        // draw ball velocity
        ctx.beginPath();
        ctx.lineWidth = 1;
        ctx.strokeStyle = "#000";
        ctx.moveTo(x(ballPosition.x), y(ballPosition.y));
        ctx.lineTo(x(ballPosition.x) + ballVelocity.x * scale, y(ballPosition.y) + ballVelocity.y * scale * -1);
        ctx.stroke();

        // draw intersect point
        ctx.beginPath();
        ctx.fillStyle = "#bbb";
        ctx.arc(x(intersectPoint.x), y(intersectPoint.y), 5, 0, 2 * Math.PI);
        ctx.fill();

        // draw ball angle
        ctx.beginPath();
        ctx.lineWidth = 2;
        ctx.strokeStyle = "#0a0";
        draw_angle(ballPosition, ballVelocity.angle());
        ctx.stroke();

        const filename = `${__dirname}/images/${tag}.png`;
        const out = fs.createWriteStream(`${filename}`)
            , stream = canvas.pngStream();

        stream.on('data', function (chunk) {
            out.write(chunk);
        });

        stream.on('end', function () {
            console.log('saved png: ' + filename);
        });
    }

    return {x: intersectPoint.x, y: intersectPoint.y};
};

module.exports = predict;